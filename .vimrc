"""""""""""""""""""""""""""""
"
" Vundle configuration
"
"""""""""""""""""""""""""""""

set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

""""""""""""""""""""""""""
"
" Additional Plugins
"
""""""""""""""""""""""""""

" Plugin 'jalcine/cmake.vim'
" Plugin 'brookhong/cscope.vim'
" Plugin 'Valloric/YouCompleteMe'
Plugin 'Shougo/neocomplete.vim'
Plugin 'Shougo/neosnippet'
Plugin 'Shougo/neosnippet-snippets'
Plugin 'honza/vim-snippets'
Plugin 'scrooloose/syntastic' 
Plugin 'bling/vim-airline'
Plugin 'SirVer/ultisnips'
Plugin 'elzr/vim-json'
Plugin 'scrooloose/nerdtree'
Plugin 'kien/ctrlp.vim'
Plugin 'klen/python-mode'
Plugin 'scrooloose/nerdcommenter'
Plugin 'mileszs/ack.vim'
Plugin 'sjl/gundo.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'tyru/open-browser.vim'
Plugin 'vim-ruby/vim-ruby'
Plugin 'tpope/vim-liquid'
Plugin 'mattn/emmet-vim'
Plugin 'pangloss/vim-javascript'
Plugin 'vim-latex/vim-latex'
"Plugin 'm2ym/rsense'


""""""""""""""""""""""""""
"
" Vim Colorschemes
"
""""""""""""""""""""""""""

Plugin 'tomasr/molokai'
Plugin 'altercation/vim-colors-solarized'
Plugin 'jpo/vim-railscasts-theme'
"Plugin 'flazz/vim-colorschemes'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line



"""""""""""""""""""""""""""""
"
" Csupport configuration
"
"""""""""""""""""""""""""""""
" let g:C_UseTool_cmake = 'yes'

"""""""""""""""""""""""""""""
"
" YouCompleteMe configuration
"
"""""""""""""""""""""""""""""

" nnoremap <leader>jd :YcmCompleter GoTo<CR>

" let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
" let g:ycm_filetype_whitelist = { 'python': 1, 'python.django': 1, 'html': 1,'htmldjango': 1, 'javascript': 1, 'sh': 1, 'vim': 1, 'cpp': 1, 'c': 1 }
" let g:ycm_show_diagnostics_ui = 0

"""""""""""""""""""""""""""""
"
" Neocomplete
"
"""""""""""""""""""""""""""""
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplete.
let g:neocomplete#enable_at_startup = 1
" Use smartcase.
let g:neocomplete#enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplete#sources#syntax#min_keyword_length = 3
" Plugin key-mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"

" For snippet_complete marker.
if has('conceal')
  set conceallevel=2 concealcursor=i
endif
" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/bundle/vim-snippets/snippets'
"""""""""""""""""""""""""""""
"
" Syntastic configuration
"
""""""""""""""""""""""""""""""

let g:syntastic_check_on_wq = 0
let g:syntastic_aggregate_errors = 1
let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚠"
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_mode_map = { "mode": "passive",
			\ "active_filetypes": [],
			\ "passive_filetypes": [] }

""""""""""""""""""""""""""""""
"
" Ultisnips configuration
"
"""""""""""""""""""""""""""""""


" Trigger configuration. Do not use <tab> if you use
" https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger="<c-d>"
let g:UltiSnipsListSnippets="<c-g>"
let g:UltiSnipsJumpForwardTrigger="<c-f>"
let g:UltiSnipsJumpBackwardTrigger="<c-b>"

" If you want :UltiSnipsEdit to split your window.
let g:UltiSnipsEditSplit="vertical"


""""""""""""""""""""""""""
"
" Ack configuration 
"
""""""""""""""""""""""""""


""""""""""""""""""""""""""""""
"
" Column 80 colorization
"
""""""""""""""""""""""""""""""
if exists('+colorcolumn')
	set colorcolumn=80
else
	au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

""""""""""""""""""""""""""""""
"
" Color Schemes
"
""""""""""""""""""""""""""""""

colorscheme railscasts
" set t_Co=256
" set background=dark
" let g:solarized_termcolors=256

""""""""""""""""""""""""""""""
"
" Airline configuration
"
""""""""""""""""""""""""""""""

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts=1
set laststatus=2 "to appear all the time

""""""""""""""""""""""""""
"
" CtrlP configuration 
"
""""""""""""""""""""""""""

let g:ctrlp_max_height = 30
set wildignore+=*.pyc
set wildignore+=*build/*
set wildignore+=*dist/*
set wildignore+=*.egg-info/*
set wildignore+=*/coverage/*

""""""""""""""""""""""""""
"
" NERDCommenter 
"
""""""""""""""""""""""""""

let g:NERDSpaceDelims = 1

""""""""""""""""""""""""""
"
" Python configuration 
"
""""""""""""""""""""""""""

" map <Leader>g :call RopeGotoDefinition()<CR>
let ropevim_enable_shortcuts = 1
let g:pymode_rope_goto_def_newwin = "snew"
let g:pymode_rope_extended_complete = 1
let g:pymode_breakpoint = 0
let g:pymode_syntax = 1
let g:pymode_virtualenv = 1
let g:pymode_rope_show_doc_bind = '<Leader>g'

""""""""""""""""""""""""""
"
" Binding 
"
""""""""""""""""""""""""""

map <Leader>n <esc>:tabprevious<CR>
map <Leader>m <esc>:tabnext<CR>

map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h


""""""""""""""""""""""""""
"
" Scripts 
"
""""""""""""""""""""""""""

nmap <F10> :read !date<CR>
nnoremap <F3> :set hlsearch!<CR>
""""""""""""""""""""""""""""""
"
" Vim options
"
""""""""""""""""""""""""""""""

"changes tabs to spaces
set tabstop=8
set softtabstop=2
set shiftwidth=2
set shiftround

set expandtab
set number
set history=700
set undolevels=700
syntax on
set hlsearch

set nobackup
set nowritebackup
set noswapfile
set foldmethod=manual
set guifont=Source\ Code\ Pro\ for\ Powerline\ 10
set guioptions=agit
set listchars=eol:\\,tab:>-,trail:~,extends:>,precedes:<

""""""""""""""""""""""""""
"
" Capitalize mapping 
"
""""""""""""""""""""""""""
if (&tildeop)
  nmap gcw guw~l
  nmap gcW guW~l
  nmap gciw guiw~l
  nmap gciW guiW~l
  nmap gcis guis~l
  nmap gc$ gu$~l
  nmap gcgc guu~l
  nmap gcc guu~l
  vmap gc gu~l
else
  nmap gcw guw~h
  nmap gcW guW~h
  nmap gciw guiw~h
  nmap gciW guiW~h
  nmap gcis guis~h
  nmap gc$ gu$~h
  nmap gcgc guu~h
  nmap gcc guu~h
  vmap gc gu~h
endif
